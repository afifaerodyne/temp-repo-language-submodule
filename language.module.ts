import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LanguageComponent } from './language.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LanguageService } from './language.service';
import { TranslateModule } from '@ngx-translate/core';
import { MonacoEditorModule } from '@materia-ui/ngx-monaco-editor';
import { SimpleSearchModule } from '../simple-search/simple-search.module';

const routes: Routes = [
  {
    path: '',
    component: LanguageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LanguageRoutingModule { }

@NgModule({
  declarations: [LanguageComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    LanguageRoutingModule,
    TranslateModule,
    MonacoEditorModule,
    SimpleSearchModule
  ],
  providers: [LanguageService],
  exports: [LanguageComponent]
})
export class LanguageModule { }
