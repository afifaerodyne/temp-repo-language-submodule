import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';

const LANGLS = 'tempLang'
@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  get API_SERVER(): string {
    return environment.baseApiUrl + 'language';
  }

  constructor(
    private httpClient: HttpClient,
  ) { }

  findAll(params?: string): Observable<ILanguageStore[]> {
    return this.httpClient.get<ILanguageStore[]>(this.API_SERVER + (params ? `?${params}` : ''))
  }

  create(payload: ILanguageStore): Observable<ILanguageStore> {
    return this.httpClient.post<ILanguageStore>(this.API_SERVER, payload)
  }

  update(code: string, payload: ILanguageStore): Observable<ILanguageStore> {
    return this.httpClient.put<ILanguageStore>(`${this.API_SERVER}/${code}`, payload)
  }

  updateBase( payload: ILanguageStore): Observable<ILanguageStore[]> {
    return this.httpClient.put<ILanguageStore[]>(`${this.API_SERVER}/base`, payload)
  }

  delete(code: string): Observable<ILanguageStore> {
    return this.httpClient.delete<ILanguageStore>(`${this.API_SERVER}/${code}`)
  }
}

export interface ILanguageStore {
  name?: string
  code?: string
  json?: { [key: string]: string }
}
