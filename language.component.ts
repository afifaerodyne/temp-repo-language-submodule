import { Component, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NavigationService } from 'src/app/services/nagivation/navigation.service';
import { ILanguageStore, LanguageService } from './language.service';

import * as enJson from 'src/assets/lang/en.json'
import * as deJson from 'src/assets/lang/de.json'
import * as jpJson from 'src/assets/lang/jp.json'

const langs = {
  en: enJson,
  de: deJson,
  jp: jpJson
}

@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.sass']
})
export class LanguageComponent implements OnInit, OnDestroy {

  ui: {
    loadedLang?: ILanguageStore
    newKey?: string
    langs?: ILanguageStore[]
    inputGroup?: FormGroup
    _base?: string[]
    base?: string[]
    baseRef?: { [inputKey: string]: string }
    applied?: boolean
    isJSON?: boolean
    showBaseInstruct?: boolean
  } = {}

  monaco = {
    options: {
      theme: 'vs',
      language: 'json'
    },
    code: '',
    isInit: false,
    dirty: false
  }

  NEWFIELD = 'new-field'

  unsub$ = new Subject()

  onSaveClicked = new EventEmitter<boolean>()

  /** Determines whether the user is editing base or translation */
  get isBase() {
    return this.ui?.loadedLang?.code === 'en'
  }

  get showBaseInstruct() {
    return this.ui.showBaseInstruct
  }
  set showBaseInstruct(val) {
    this.ui.showBaseInstruct = val
  }

  constructor(
    public _t: TranslateService,
    private langService: LanguageService,
    private formBuilder: FormBuilder,
    private navService: NavigationService
  ) { }

  objKeys = Object.keys

  ngOnInit(): void {
    this.refreshComponent()
  }

  async refreshComponent(selectLangCode?: string, newLangs?: ILanguageStore[]) {
    if (newLangs?.length) {
      this.ui.langs = newLangs
    }
    else {
      const langs = await this.langService.findAll('withJSON=true').toPromise()

      if (!langs?.length) return
      this.ui.langs = langs
    }

    this.selectLang(selectLangCode || this.ui?.loadedLang?.code || 'en')
  }

  ngOnDestroy(): void {
    this.unsub$.next()
    this.unsub$.unsubscribe()
  }

  toggleJSON() {
    this.ui.isJSON = !this.ui.isJSON
    if (this.ui.isJSON)
      this.initMonaco(this.ui.loadedLang.json)
  }

  onLangChange(langCode: string) {
    console.log('onLangChange().langCode: ', langCode)

    this.selectLang(langCode)

    if (this.isBase) { this.ui.isJSON = false }
  }

  selectLang(langCode: string) {
    this.ui.loadedLang = this.ui.langs.find(lang => lang.code === langCode)
    this._t.use(langCode)
    this.genInputGroup()
  }

  _initInputGroup() {
    const group = this.formBuilder.group({
      "component-init": ['']
    })

    group.valueChanges.pipe(takeUntil(this.unsub$.asObservable())).subscribe(form => {
      if (this.isBase) {
        this.processBaseChanges(form)
      }
    })

    return group
  }

  processBaseChanges(form: any) {
    // console.log('processBaseChanges().form: ', form)
  }

  /** Generate control name */
  genControlName(key: string) {
    return 'input-' + key.split(' ').join('-')
  }

  genInputGroup() {
    const json = this.ui.loadedLang.json
      , baseArr = Object.keys(json)

    const inputGroup = this._initInputGroup()

    baseArr.map(key => {
      const val = json[key] || ''
        , controlName = this.genControlName(key)

      this.ui.baseRef = this.ui.baseRef || {}
      this.ui.baseRef[controlName] = key

      inputGroup.addControl(`${controlName}`, new FormControl({value: val, disabled: this.isBase ? true : false}))
    })

    if (this.isBase) {
      inputGroup.addControl(this.NEWFIELD, new FormControl(''))
    }

    this.ui.inputGroup = inputGroup

    this.ui._base = baseArr.sort((a, b) => {
      return b.toLowerCase() > a.toLowerCase() ? -1 : b.toLowerCase() < a.toLowerCase() ? 1 : 0
    })
  }

  initMonaco(json: any) {
    this.monaco.isInit = true
    this.monaco.code = JSON.stringify(json, null, '\t')
    this.monaco.dirty = false
    setTimeout(() => {
      this.monaco.isInit = false
    }, 100)
  }

  /**
   * ngModelChange event from monaco editor
   * @param evt string
   */
  onMonacoCodeChanged(evt: string) {
    this.processUserEdit(evt)
  }

  /**
   * Cancels monaco edit
   */
  cancelMonacoEdit(clickEvent: any) {
    this.ui.isJSON = false
    this.initMonaco(this.ui.loadedLang.json)
  }

  /** 
   * Apply changes to monaco editor
   */
  async applyMonacoChanges(clickEvent: any) {
    let parsed
    try {
      const changes = this.monaco.code
      parsed = JSON.parse(changes)
    }
    catch (err) {
      /** Error when parsing JSON */
      return
    }

    const og = this.ui.loadedLang.json
    let added = {}, removed = {}

    Object.keys(parsed).map(key => {
      const val = parsed[key]

      if (!val) parsed[key] = key

      if (!og[key]) added[key] = key
    })

    Object.keys(og).map(key => {
      if (!parsed[key]) removed[key] = key
    })

    /** TODO: Present added and removed to user for final confirmation. Warn them that it will update all languages */
    console.log('added: ', added)
    console.log('removed: ', removed)

    /** Assumes that everything is ok and updates the lang */
    try {
      const updatedLangs = await this.langService.updateBase({ json: parsed }).toPromise()

      this.ui.isJSON = false
      this.monaco.dirty = false

      this.refreshComponent(null, updatedLangs)
    }
    catch (err) {
      console.error('applyMonacoChanges().updateBase().Err: ', err)
    }
  }

  /**
   * Filters out monaco editor change when it's initialized
   * @param changes event from monacoeditorchanged
   */
  processUserEdit(changes: string) {
    if (this.monaco.isInit) return

    this.monaco.dirty = true
    this.monaco.code = changes
  }

  async applyLang(langCode?: string) {
    // console.log('applyLang.code: ', langCode)
    /** Process changes in input to add into loaded lang json */

    const lang = langCode === this.ui.loadedLang.code ? this.ui.loadedLang : this.ui.langs.find(lang => lang.code === langCode)
      , controls = this.ui.inputGroup.controls

    Object.keys(controls).map(inputKey => {
      const base = this.ui.baseRef[inputKey]
        , control = this.ui.inputGroup.controls[inputKey]

      if (base && control.dirty) {
        lang.json[base] = control.value
      }
    })

    this.applyTranslation(lang.code)

    this.ui.applied = true

    const updatedLang = await this.langService.update(lang.code, { json: lang.json }).toPromise()

    this.ui.langs.map(lang => {
      if (lang.code === updatedLang.code) {
        lang = updatedLang
      }
    })

    this.ui.inputGroup.markAsPristine()
  }

  applyTranslation(langCode: string) {
    const lang = this.ui.langs.find(lang => lang.code === langCode)
    this._t.setTranslation(langCode, lang.json, false)
    this._t.use(langCode)
  }

  async saveLang(langToSave?: ILanguageStore) {
    console.log('saveLang.toSave: ', langToSave)

    if (this.isBase) {
      /** Simulate updating bases in UI first */
      const json = this.ui.loadedLang.json
      
      console.log('json: ', json)

      /** TODO:
       * - Present modal on the changes that's going to happen
       * - Will probably need to separate between edited and original to make the comparison
       */
      /** Assumes that everything is ok and updates the lang */
      try {
        const updatedLangs = await this.langService.updateBase({ json }).toPromise()

        this.refreshComponent(null, updatedLangs)
      }
      catch (err) {
        console.error('applyMonacoChanges().updateBase().Err: ', err)
      }
    }
    else {
      const lang = langToSave || this.ui.loadedLang
      if (!this.ui.applied) this.applyLang(lang.code)

      this.navService.back()
    }
  }

  /** Add new base */
  addNewBase(evt: any) {
    const newBase = this.ui.inputGroup.controls[this.NEWFIELD].value

    if (!newBase) return

    /** Temporarily update in UI */
    const json = this.ui.loadedLang.json

    json[newBase] = newBase

    this.genInputGroup()
  }

  async initLanguage() {
    const defaultLangs = {
      'en': {
        name: 'English',
        json: {}
      },
      'jp': {
        name: '日本語',
        json: {}
      },
      'de': {
        name: 'Deutsch',
        json: {}
      }
    }

    await Object.keys(defaultLangs).reduce(async (p, key) => {
      await p

      const langJson = (('default' in langs[key]) && Object.keys(langs[key]).length === 1) ? langs[key].default : langs[key]
      
      const payload: ILanguageStore = {
        name: defaultLangs[key].name,
        code: key,
        json: langJson
      }

      if (!langJson) {
        console.error('Default language not found. Unable to initialize language code: ', key)
        return p
      }

      try {
        await this.langService.create(payload).toPromise()
      }
      catch (err) {
        console.error('initLanguage().create().err: ', err)
      }

      return p
    }, Promise.resolve())

    location.reload()
  }
}
